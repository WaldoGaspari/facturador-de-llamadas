require 'rspec'
require 'date'
require_relative '../model/llamada'
require_relative '../model/llamada_internacional'

describe 'LlamadaInternacional' do  

  it 'si se realiza una llamada internacional de 1 min de duracion a Norteamerica deberia costar 10' do
    @llamada = LlamadaInternacional.new("54 011 4444 4444", "52 011 5555 6666", "20190211;14:30", "20190211;14:31")
    expect(@llamada.obtener_costo).to eq 10
  end

  it 'si se realiza una llamada internacional de 1 min de duracion a un pais de Sudamerica deberia costar 6' do
    @llamada = LlamadaInternacional.new("54 011 44444 444", "55 011 5555 6666", "20190211;14:30", "20190211;14:31")
    expect(@llamada.obtener_costo).to eq 6
  end

  it 'si se realiza una llamada internacional de 1 min de duracion a cualquier otro pais del mundo deberia costar 15' do
    @llamada = LlamadaInternacional.new("54 011 44444 444", "39 011 5555 6666", "20190211;14:30", "20190211;14:31")
    expect(@llamada.obtener_costo).to eq 15
  end

end