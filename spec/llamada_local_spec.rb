require 'rspec'
require 'date'
require_relative '../model/llamada'
require_relative '../model/llamada_local'

describe 'LlamadaLocal' do  

  it 'si se realiza una llamada local de 1 min de duracion un dia de semana habil entre las 8 y 20 hs deberia costar 3.20' do
    @llamada = LlamadaLocal.new("54 011 4444 1111", "54 011 5555 6666", "2019021;14:30", "20190211;14:31")
    expect(@llamada.obtener_costo).to eq 3.20
  end

  it 'si se realiza una llamada local de 1 min de duracion un dia de semana habil pasadas las 20 hs deberia costar 1.8' do
    @llamada = LlamadaLocal.new("54 011 4444 1111", "54 011 5555 6666", "20190211;22:30", "20190211;22:31")
    expect(@llamada.obtener_costo).to eq 1.80
  end

  it 'si se realiza una llamada local de 1 min de duracion un sabado deberia costar 2.1' do
    @llamada = LlamadaLocal.new("54 011 4444 1111", "54 011 5555 6666", "20190210;14:30", "20190210;14:31")
    expect(@llamada.obtener_costo).to eq 2.10
  end

end