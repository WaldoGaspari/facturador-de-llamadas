require 'rspec'
require 'date'
require_relative '../model/llamada'
require_relative '../model/llamada_nacional'
require_relative '../model/llamada_local'
require_relative '../model/llamada_internacional'
require_relative '../model/registro_de_llamadas'

describe 'RegistroDeLlamadas' do

  let(:registro) { RegistroDeLlamadas.new }

  it 'si se realiza una llamada internacional, una local y una nacional el registro de llamadas deberia tener 3' do
    registro.agregar_llamada(LlamadaLocal.new("54 011 4444 1111", "54 011 5555 6666", "2019021;14:30", "20190211;14:31"))
    registro.agregar_llamada(LlamadaNacional.new("54 011 4444 4444", "54 314 5555 6666", "20190211;14:30", "20190211;14:31"))
    registro.agregar_llamada(LlamadaInternacional.new("54 011 4444 4444", "52 011 5555 6666", "20190211;14:30", "20190211;14:31"))
    expect(registro.obtener_llamadas.size).to eq 3
  end

  it 'si un cliente realiza una llamada internacional, una local y una nacional en el mes de enero deberia abonar 133.20 pesos' do
    registro.agregar_llamada(LlamadaLocal.new("54 011 4444 4444", "54 011 5555 6666", "20190208;14:30", "20190211;14:31"))
    registro.agregar_llamada(LlamadaNacional.new("54 011 4444 4444", "54 314 555 56666", "20190211;14:30", "20190211;14:31"))
    registro.agregar_llamada(LlamadaInternacional.new("54 011 4444 4444", "52 011 5555 6666", "20190211;14:30", "20190211;14:31"))
    expect(registro.calcular_facturacion_de_cliente("54 011 4444 4444", 2019, 02)).to eq 133.20
  end

  it 'si un cliente no realiza ningun tipo de llamadas en el mes de enero deberia abonar solo 100 pesos de mensualidad' do
    registro.agregar_llamada(LlamadaLocal.new("54 011 4444 4444", "54 011 5555 6666", "20190208;14:30", "20190211;14:31"))
    registro.agregar_llamada(LlamadaNacional.new("54 011 4444 4444", "54 314 5555 6666", "20190211;14:30", "20190211;14:31"))
    registro.agregar_llamada(LlamadaInternacional.new("54 011 4444 4444", "52 011 5555 6666", "20190211;14:30", "20190211;14:31"))
    expect(registro.calcular_facturacion_de_cliente("54 011 4444 5555", 2019, 02)).to eq 100
  end

  it 'si se borran todas las llamadas que tiene el registro deberia quedar vacio' do
    registro.agregar_llamada(LlamadaLocal.new("54 011 4444 4444", "54 011 5555 6666", "20190208;14:30", "20190211;14:31"))
    registro.agregar_llamada(LlamadaNacional.new("54 011 4444 4444", "54 314 5555 6666", "20190211;14:30", "20190211;14:31"))
    registro.agregar_llamada(LlamadaInternacional.new("54 011 4444 4444", "52 011 5555 6666", "20190211;14:30", "20190211;14:31"))
    expect(registro.eliminar_llamadas).to be_truthy
  end

end