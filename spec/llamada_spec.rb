require 'rspec'
require 'date' 
require_relative '../model/llamada'

describe 'Llamada' do  
   
  it 'si se realiza una llamada con numero de origen 5401144441111 deberia tener registrado dicho numero' do
    @llamada = Llamada.new("54 011 4444 1111", "54 011 5555 6666", "20190211;14:30", "20190211;14:31")
    expect(@llamada.obtener_nro_origen).to eq "5401144441111"
  end

  it 'si se realiza una llamada con numero de destino 5401155556666 deberia tener registrado dicho numero' do
    @llamada = Llamada.new("54 011 4444 1111", "54 011 5555 6666", "20190211;14:30", "20190211;14:31")
    expect(@llamada.obtener_nro_destino).to eq "5401155556666"
  end

  it 'si se realiza una llamada con la misma fecha y hora de inicio y de fin deberia tener costo 0' do
    @llamada = Llamada.new("54 011 4444 1111", "54 011 5555 6666", "20190211;14:30", "20190211;14:31")
    expect(@llamada.obtener_costo).to eq 0.0
  end

  it 'si se realiza una llamada a un numero cuya longitud es de 16 deberia poder realizarse satisfactoriamente' do
    @llamada = Llamada.new("54 011 4444 1111", "54 011 5555 6666", "20190211;14:30", "20190211;14:31")
    expect(@llamada.obtener_costo).to eq 0.0
  end

  it 'si se realiza una llamada a un numero cuya longitud es mayor a 16 NO deberia poder realizarse la llamada' do
    expect{Llamada.new("54 011 4444 1111", "54 011 5555 6666 66", "20190211;14:30", "20190211;14:31")}.to raise_error ArgumentError
  end

  it 'si se realiza una llamada a un numero cuya longitud es menor a 16 NO deberia poder realizarse la llamada' do
    expect{Llamada.new("54 011 4444 1111", "54 011 5555 666", "20190211;14:30", "20190211;14:31")}.to raise_error ArgumentError
  end

end