require 'rspec'
require 'date'
require_relative '../model/llamada'
require_relative '../model/llamada_nacional'

describe 'LlamadaNacional' do  

  it 'si se realiza una llamada nacional de 1 min de duracion deberia costar 20 pesos' do
    @llamada = LlamadaNacional.new("54 011 4444 4444", "54 314 5555 6666", "20190211;14:30", "20190211;14:31")
    expect(@llamada.obtener_costo).to eq 20
  end

  it 'si se realiza una llamada nacional de 6 min de duracion deberia costar 21.5 pesos' do
    @llamada = LlamadaNacional.new("54 011 4444 5555", "54 314 5555 6666", "20190211;22:30", "20190211;22:36")
    expect(@llamada.obtener_costo).to eq 101.5
  end

end