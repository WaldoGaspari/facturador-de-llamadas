require 'sinatra'
require 'sinatra/json'
require 'json'
require_relative '../facturador-de-llamadas/model/registro_de_llamadas'
require_relative '../facturador-de-llamadas/model/llamada'
require_relative '../facturador-de-llamadas/model/llamada_local'
require_relative '../facturador-de-llamadas/model/llamada_nacional'
require_relative '../facturador-de-llamadas/model/llamada_internacional'

registro = RegistroDeLlamadas.new

post '/llamadas' do
  json_parseado = JSON.parse(request.body.read) 
  begin
    if(json_parseado['numero_destino'].start_with?("54"))
      if(json_parseado['numero_destino'].include? "011")
        registro.agregar_llamada(LlamadaLocal.new(json_parseado['numero_origen'], json_parseado['numero_destino'], json_parseado['fechahora_inicio'], json_parseado['fechahora_fin']))
      else
        registro.agregar_llamada(LlamadaNacional.new(json_parseado['numero_origen'], json_parseado['numero_destino'], json_parseado['fechahora_inicio'], json_parseado['fechahora_fin']))
      end
    else
      registro.agregar_llamada(LlamadaInternacional.new(json_parseado['numero_origen'], json_parseado['numero_destino'], json_parseado['fechahora_inicio'], json_parseado['fechahora_fin']))
    end
    status 201
    json({ costo_llamada: registro.obtener_llamadas.last.obtener_costo })
  rescue StandardError => e
    halt 500, json({ error: e.message})
  end
end

get '/facturacion' do
  begin
    fecha = params[:mes].to_s.chars.each_slice(4).map { |posicion| posicion.join.to_i }
    resultado = registro.calcular_facturacion_de_cliente(params[:numero], fecha[0], fecha[1])
    status 200
    json({ cantidad_llamadas: registro.cantidad_de_llamadas, costo_total: resultado })
  rescue StandardError => e
    halt 500, json({ error: e.message})
  end
end

post '/reset' do
  begin
    resultado = registro.eliminar_llamadas
    status 200
    json({ reset: resultado })
  rescue StandardError => e
    halt 500, json({ error: e.message})
  end
end
