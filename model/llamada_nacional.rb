class LlamadaNacional < Llamada

  def initialize(nro_origen, nro_destino, fecha_hora_inicio, fecha_hora_fin)
    super(nro_origen, nro_destino, fecha_hora_inicio, fecha_hora_fin)
    @costo = calcular_costo_de_llamada
  end

  def calcular_costo_de_llamada
  	minutos_de_llamada = @fecha_hora_fin.min - @fecha_hora_inicio.min
    if(minutos_de_llamada <= 5)
      @costo = minutos_de_llamada * 20
    else
      minutos_adicional = minutos_de_llamada - 5
      @costo = (20 * 5) + (minutos_adicional * 1.5)
    end
  end
end