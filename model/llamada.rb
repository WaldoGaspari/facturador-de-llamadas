require 'date' 
class Llamada

  def initialize (nro_origen, nro_destino, fecha_hora_inicio, fecha_hora_fin)
    @nro_origen = validar_numero(nro_origen)
    @nro_destino = validar_numero(nro_destino) 
    @fecha_hora_inicio = DateTime.strptime(fecha_hora_inicio, '%Y%m%d;%H:%M')
    @fecha_hora_fin = DateTime.strptime(fecha_hora_fin, '%Y%m%d;%H:%M')
    @costo = calcular_costo_de_llamada
  end

  def obtener_nro_origen
  	@nro_origen
  end

  def obtener_nro_destino
  	@nro_destino
  end

  def obtener_fecha_hora_inicio
    @fecha_hora_inicio
  end

  def obtener_costo
    @costo
  end

  def calcular_costo_de_llamada
    @costo = 0.0
  end

  def validar_numero(numero)
    numero_a_validar = numero.delete ' '
    if(numero_a_validar.size != 13)
      raise ArgumentError.new('El numero ingresado es incorrecto. No cumple con la longitud deseada.')
    else
      numero_a_validar
    end
  end

end