class LlamadaInternacional < Llamada

  def initialize(nro_origen, nro_destino, fecha_hora_inicio, fecha_hora_fin)
    super(nro_origen, nro_destino, fecha_hora_inicio, fecha_hora_fin)
    @costo = calcular_costo_de_llamada
  end

  def calcular_costo_de_llamada
  	minutos_de_llamada = @fecha_hora_fin.min - @fecha_hora_inicio.min
    if(@nro_destino.start_with?("52") || @nro_destino.start_with?("0"))
      @costo = minutos_de_llamada * 10
    else
      if(@nro_destino.start_with?("5") || @nro_destino.start_with?("9"))
      @costo = minutos_de_llamada * 6
      else
        @costo = minutos_de_llamada * 15
      end
    end
  end
end