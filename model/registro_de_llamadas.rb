class RegistroDeLlamadas

  attr_accessor :cantidad_de_llamadas

  def initialize
    @llamadas = Array.new
  end

  def agregar_llamada(llamada)
    @llamadas.push(llamada)
  end

  def obtener_llamadas
    @llamadas
  end

  def calcular_facturacion_de_cliente(nro_cliente, anio, mes)
    @cantidad_de_llamadas = 0
    resultado = 100
    nro_cliente_sin_espacios = nro_cliente.delete ' '
    @llamadas.each { |llamada| 
        if(llamada.obtener_nro_origen == nro_cliente_sin_espacios && llamada.obtener_fecha_hora_inicio.month == mes && llamada.obtener_fecha_hora_inicio.year == anio) 
          @cantidad_de_llamadas += 1
          resultado += llamada.obtener_costo
        end 
    }
    resultado
  end

  def eliminar_llamadas
  	@llamadas.clear
  	@llamadas.empty?
  end

end