class LlamadaLocal < Llamada

  def initialize(nro_origen, nro_destino, fecha_hora_inicio, fecha_hora_fin)
    super(nro_origen, nro_destino, fecha_hora_inicio, fecha_hora_fin)
    @costo = calcular_costo_de_llamada
  end

  def calcular_costo_de_llamada
  	minutos_de_llamada = @fecha_hora_fin.min - @fecha_hora_inicio.min
    if(@fecha_hora_inicio.cwday.between?(1,5))
      if(@fecha_hora_inicio.hour.between?(8,20))
        @costo = minutos_de_llamada * 3.20
      else
        @costo = minutos_de_llamada * 1.80
      end
    else
      @costo = minutos_de_llamada * 2.10
    end
  end
end